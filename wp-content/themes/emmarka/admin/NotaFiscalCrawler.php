<?php
class NotaFiscalCrawler {
    /**
     * URLs das ações
     * @var array
     */
    protected $urls = array(
        'captcha' => 'http://www.e-danfe.com/home/captcha_json',
        'send' => 'http://www.e-danfe.com/index.php/home/send',
        'home' => 'http://www.e-danfe.com/home'
    );

    /**
     * Número da nota fiscal
     * @var string
     */
    protected $nfe = null;
    

    /**
     * Path do arquivo de cookie
     * @var string
     */
    protected $cookieFile = null;

    /**
     * Error
     */
    public $error = false;

    /**
     * Lista de errors
     */
    public $errorList = array();

    /**
     * Path da pasta do cookie
     */
    private $cookiePath;

    /**
     * Conteúdo do cookie
     */
    private $cookieContent = null;

    /**
     * Conteúdo do pdf do danfe
     */
    public $pdfContent = null;


    public function __construct()
    {
        $this->cookiePath = __DIR__ . "/../../../nfe-cookies";
        
        if (isset($_SESSION['nfe_cookie']) && file_exists($this->cookiePath . $_SESSION['nfe_cookie'] . ".txt"))
        {
            $this->cookieFile = realpath($this->cookiePath . $_SESSION['nfe_cookie'] . ".txt");
        }
    }

    private function generateCookieFile()
    {
        if (!is_null($this->cookieFile)) return false;

        // generate cookie name
        $cookieName = md5(uniqid() . time() . rand(1, 999));

        // create file if not exists
        $cookiePath = $this->cookiePath . $cookieName . ".txt";
        if (!file_exists($cookiePath)) touch($cookiePath);
        $this->cookieFile = realpath($cookiePath);

        // save cookie name in session
        $_SESSION['nfe_cookie'] = $cookieName;
    }

    public function getViewFields()
    {
        // generate cookie file
        $this->generateCookieFile();

        // request
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->urls['home']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFile);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFile);

        $result = curl_exec($ch);
        preg_match_all('/<input type="hidden" name="dt_token" value="(.*)" \/>/im', $result, $matches);
        $dt_token = $matches[1][0];

        //
        curl_setopt($ch, CURLOPT_URL, $this->urls['captcha']);
        $result = curl_exec($ch);
        curl_close($ch);

        // serialize json response
        $result_json = json_decode($result);
        $result_json->dt_token = $dt_token;

        // save cookie data on file
        $this->cookieContent = file_get_contents($this->cookieFile);

        // remove file cookie
        $this->removeCookieFile();

        return $result_json;
    }

    private function multipartBuildQuery($fields, $boundary)
    {
        $retval = '';
        foreach($fields as $key => $value){
            $retval .= "--$boundary\nContent-Disposition: form-data; name=\"$key\"\n\n$value\n";
        }
        $retval .= "--$boundary--";
        return $retval;
    }

    public function view($form_data, $nfeName, $captchaName)
    {
        $this->nfe = $form_data[$nfeName];
        $captcha = $form_data[$captchaName];

        // generate cookie file
        $this->generateCookieFile();
        file_put_contents($this->cookieFile, base64_decode($_POST['cookie']));

        $json = unserialize(base64_decode($_POST['json']));
        $data = array(
            'dt_token' => $json->dt_token,
            'chave' => $this->nfe,
            'chave-captcha' => $captcha,
            'cookie' => $json->cookie,
            'viewstate' => $json->viewstate,
            'eventvalidation' => $json->eventvalidation,
            'token' => $json->token,
            'captchasom' => $json->captchasom,
            'tipo' => '2'
        );

        $boundary = '----WebKitFormBoundaryf9RCcgJEKpnBVb5t';
        $body = $this->multipartBuildQuery($data, $boundary);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: multipart/form-data; boundary=$boundary"));
        curl_setopt($ch, CURLOPT_URL, $this->urls['send']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFile);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFile);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        $result_json = json_decode($result);

        if ($result_json->retorno == 0) {
            throw new Exception($result_json->aviso);
        }

        curl_setopt($ch, CURLOPT_URL, 'http://www.e-danfe.com/NFe.php?chave=' . $this->nfe);
        curl_setopt($ch, CURLOPT_POST, false);

        $result = curl_exec($ch);
        curl_close($ch);

        $this->pdfContent = base64_encode($result);

        // remove file cookie
        $this->removeCookieFile();

        return true;
    }

    private function removeCookieFile()
    {
        // delete session cookie
        @unlink($this->cookieFile);
        unset($_SESSION['nfe_cookie']);
    }

    public function getCookieContent()
    {
        return $this->cookieContent;
    }
}
?>