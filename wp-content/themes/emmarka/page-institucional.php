<?php get_header(); ?>
	
	<?php
	while (have_posts()) :
		the_post(); 

		$fotos = get_field( "fotos" );
	?>

	
	

	<main id="institucional">
		<div class="container interna">
			<h1 class="title"><span><?php the_title();?></span></h1>

			<div class="row">
				<div class="col s12 m7 box">
					<div><?php the_content();?></div>
					<div>
					<div id="slider-institucional" class="instituto">
						<a href="javascript:;" class="arrow left"><i class="fa fa-angle-left"></i></a>

						<div id="slider1" class="cycle-slideshow" data-cycle-slides="> div" data-cycle-fx="scrollHorz" data-cycle-timeout="3000" data-cycle-slides=".item" data-cycle-next=".instituto .right" data-cycle-prev=".instituto .left">
						
							<?php if (is_array($fotos)) :
								foreach ($fotos as $foto) : ?>

								<div><a href="#" class="item"><img src="<?php echo $foto['image']['sizes']['medium_large'] ?>" alt="<?php echo $foto['image']['title'] ?>"></a></div>

							<?php endforeach;
							 endif; ?> 
						</div>
						<a href="javascript:;" class="arrow right"><i class="fa fa-angle-right"></i></a>
					</div>
					</div>
				</div>
				<div class="col s12 m4 offset-m1 box">
					<div>
						<h2>Missão</h2>
						<?php
							echo get_field('missao');
						?>
					</div>
					<div>
						<h2>Visão</h2>
						<?php
							echo get_field('visao');
						?>
					</div>
					<div>
						<h2>Valores</h2>
						<?php
							echo get_field('valores');
						?>
					</div>
				</div>
			</div>
			
		</div>
	</main>
	<?php endwhile; ?>

<?php get_footer(); ?>