<?php get_header(); ?>

	<main class="formulario interna" id="fale-conosco">
		<div class="container interna">
			<h1 class="title"><span><?php the_title(); ?></span></h1>

			<p class="subtitulo">Telefones</p>
			<ul class="list">
				<li><span>EMMARKA-CE - 0800.725.9060</span></li>
				<li><span>EMMARKA-PE - 0800.729.6966</span></li>
				<li><span>EMMARKA-PB - 0800.083.4100</span></li>
				<li><span>EMMARKA-RN - (84) 3272.2772</span></li>
				<li><span>EMMARKA-MA - (98) 3301.2500</span></li>
			</ul>

			<p class="subtitulo">Informe os seus dados</p>

			<?php echo do_shortcode('[contact-form-7 id="76" title="Fale Conosco"]') ?>

			<div id="mapa">
				<div class="row">
					<div class="col s12 m6 lg6">
						<p class="subtitulo">Localização</p>
						<p class="mapa-endereco">Rua Barão do Rio Branco, 2287 - José Bonifacio, Fortaleza - CE</p>
					</div>
					<div class="col s12 m6 lg6">
						<div class="input-field">
							<select class="mapa-cidades">
									<option value="0">FORTALEZA - CE</option>
									<option value="1">RECIFE - PE</option>
									<option value="2">BAYEUX - PB</option>
									<option value="3">PARNAMIRIM - RN</option>
									<option value="4">SÃO LUíS - MA</option>
							</select>
						</div>
					</div>
				</div>
				<div>
					<iframe class="mapa-google" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3981.3294167148943!2d-38.53413678524111!3d-3.738211797280295!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c74903fdd8cce5%3A0xb80cacfd34b08d26!2sEMMARKA+-+Destribuidora+de+Medicamentos!5e0!3m2!1spt-BR!2sbr!4v1479920387526" width="970" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>

		</div>
	</main>

<?php get_footer(); ?>
