<?php get_header(); ?>

	<main class="formulario" id="pedido-eletronico">
		<div class="container interna">
			<h1 class="title"><span><?php the_title(); ?></span></h1>
			<p class="subtitulo">Informe os seus dados</p>
			
			<?php echo do_shortcode('[contact-form-7 id="72" title="Pedido eletrônico"]') ?>
			

		</div>
	</main>

<?php get_footer(); ?>>