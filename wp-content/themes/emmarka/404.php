<?php  get_header(); ?>


	<main id="404">
		<div class="container interna">

			<h1 class="text-center">ERRO 404</h1>
			<p class="text-center">A página não foi encontrada, tente novamente ou <a href="<?php echo home_url('/');?>" title="Página inicial">clique aqui para ir para a página inicial</a>.</p>
		</div>
	</main>



<?php get_footer(); ?>