<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Emmarka</title>

    <?php wp_head(); ?>
</head>
<body>
    <header id="header">
        <nav id="menu-main">
            <div class="container">
                <div id="menu-aux">
                    <ul class="right">
                        <li><a href="https://www63.bb.com.br/portalbb/boleto/boletos/hc21e,802,3322,10343.bbx" target="blank" class="large waves-effect waves-light color-dark">2° VIA BOLETO</a></li>
                        <li><a href="<?php echo home_url('/nfe');?>" class="large waves-effect waves-light color-light">CONSULTA NF-e</a></li>
                        <li><a href="https://www.facebook.com/pages/Plusfarma-Distribuidora-De-Produtos-Farmac%C3%AAuticos/1632312317003913?fref=ts" target="blank" class="waves-effect waves-light color-baby"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                        <li><button class="waves-effect waves-light btn-search color-dark"><i class="fa fa-search" aria-hidden="true"></i></button></li>
                    </ul>
                </div>


                <div class="nav-wrapper">
                    <a href="<?php echo home_url('/');?>" class="brand-logo"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/logo-emmarka.png" alt="Emmarka"></a>
                    <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="<?php echo home_url('/');?>">HOME</a></li>
                        <li><a href="<?php echo home_url('/institucional');?>">INSTITUCIONAL</a></li>
                        <li><a href="<?php echo home_url('/pedido-eletronico');?>">PEDIDO ELETRÔNICO</a></li>
                        <li><a href="<?php echo home_url('/area-fornecedor');?>">DOCUMENTAÇÃO</a></li>
                        <li><a href="<?php echo home_url('/galeria');?>">GALERIA DE IMAGENS</a></li>
                        <li class="submenu">
                            <a href="#" id="contato">CONTATO</a>
                            <ul id="sub">
                                <li><a href="<?php echo home_url('/trabalhe-conosco');?>">TRABALHE CONOSCO</a></li>
                                <li><a href="<?php echo home_url('/fale-conosco');?>">FALE CONOSCO</a></li>
                            </ul>
                        </li>
                    </ul>

                    <ul class="side-nav" id="mobile-demo">
                        <li>
                            <div class="search-wrapper">
                                <form class="search-mobile" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                                    <input id="search" type="text" placeholder="Pesquise aqui..." name="s" value="<?php the_search_query(); ?>">
                                    <button class="btn-search"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                        </li>
                        <li><a href="<?php echo home_url('/');?>">HOME</a></li>
                        <li><a href="<?php echo home_url('/institucional');?>">INSTITUCIONAL</a></li>
                        <li><a href="<?php echo home_url('/pedido-eletronico');?>">PEDIDO ELETRÔNICO</a></li>
                        <li><a href="<?php echo home_url('/area-fornecedor');?>">DOCUMENTAÇÃO</a></li>
                        <li><a href="<?php echo home_url('/galeria');?>">GALERIA DE IMAGENS</a></li>
                        <li><a href="<?php echo home_url('/trabalhe-conosco');?>">TRABALHE CONOSCO</a></li>
                        <li><a href="<?php echo home_url('/fale-conosco');?>">FALE CONOSCO</a></li>
                        <li><a href="javascript;" class="waves-effect btn waves-light color-dark">2° VIA BOLETO</a></li>
                        <li><a href="<?php echo home_url('/nfe');?>" class="waves-effect btn waves-light color-light">CONSULTA NF-e</a></li>
                        <li><a href="javascript;" class="waves-effect btn waves-light color-baby">FACEBOOK</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <form class="search-bar" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                <div class="input-field">
                    <input placeholder="Pesquise aqui ..." id="pesquisa" type="text" name="s" value="<?php the_search_query(); ?>">
                    <i class="fa fa-close btn-search"></i>
                </div>
            </form>
        </div>

    </header>
