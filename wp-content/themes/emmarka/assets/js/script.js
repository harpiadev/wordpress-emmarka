$(".button-collapse").sideNav();
$(".dropdown-button").dropdown();
$('select').material_select();
// $(document).ready(function() {
//   });

$('.search-bar').hide();
$( ".btn-search" ).click(function() {
	 $('.search-bar').slideToggle("fast");
});


// Mascara para o formulario (data e telefone)
$(document).ready(function(){
  $('.date').mask('00/00/0000');
  $('.phone_with_ddd').mask('(00) 00000-0000');
  $('.phone_with_fixed_ddd').mask('(00) 0000-0000');
  $('.CPF').mask('000.000.000-00');
  $('.CNPJ').mask('00.000.000/0000-00');

});


/**
 * Alterar tamanho da imagem dos fornecedores de acordo com o tamanho da página
 =============================================================================== */

var $fornecedoresContainer = $('#home .marcas .container-marcas .template-marcas');
var resizeFornecedoresIcons = function() {
	var widthIcon = function() {
		var width = $('#home .marcas .container-marcas .template-marcas').width(),
			qtd;

		if (width >= 900) {
			qtd = 5;
		} else if (width >= 870) {
			qtd = 4;
		} else if (width >= 650) {
			qtd = 4;
		} else {
			qtd = 2;
		}

		return parseInt(width/qtd);
	}

	$('#responsive-fornecedores').remove();

	var style = document.createElement('style');
	style.id = 'responsive-fornecedores';
	style.type = 'text/css';
	style.innerHTML = '#home .marcas .container-marcas .template-marcas .item { width: ' + widthIcon() + 'px !important; }';
	document.getElementsByTagName('head')[0].appendChild(style);
}

if ($fornecedoresContainer.length > 0) {
	resizeFornecedoresIcons();
}





/**
 * Alterar endereço do mapa em contatos
 =============================================================================== */
var $endereco = $('.mapa-endereco');
var $mapa = $('.mapa-google');

 $('.mapa-cidades').on('change', function() {
	var result = $(this).val();

	var adress = [
		'Rua Barão do Rio Branco, 2287 - José Bonifacio, Fortaleza - CE',
		'Rua Antônio Luiz Soares, 217 - Boa Viagem, Recife - PE',
		'Av. Liberdade, 1789 - São Bento, Bayeux - PB',
		"Rua Dr. Carlos Matheus, 76 - Centro, Parnamirim - RN",
		"Rua Seis, 54 - Jardim Eldorado, São Luís - MA"
	];
	var map = [
		"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3981.3294167148943!2d-38.53413678524111!3d-3.738211797280295!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c74903fdd8cce5%3A0xb80cacfd34b08d26!2sEMMARKA+-+Destribuidora+de+Medicamentos!5e0!3m2!1spt-BR!2sbr!4v1479920387526",
		"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3949.6060028103666!2d-34.919152285219596!3d-8.141530594139498!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7ab1e28ae3e244f%3A0xe6a3cd9542040a79!2sEMMARKA+-+PE+DISTRIBUIDORA+DE+MEDICAMENTOS+LTDA!5e0!3m2!1spt-BR!2sbr!4v1479920634191",
		"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3959.019680076168!2d-34.92211258522603!3d-7.123716194855862!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7ace89b70bc69cb%3A0x2d794dc84117be79!2sEmmarka+PB+Dist+de+Medicamentos!5e0!3m2!1spt-BR!2sbr!4v1479920922402",
		"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15874.550461499637!2d-35.261268!3d-5.90641!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7b256ffcc5e3a5b%3A0xb5e3eed0b1698630!2sRua+Dr.+Carlos+Matheus%2C+76+-+Centro%2C+Parnamirim+-+RN%2C+59140-250%2C+Brasil!5e0!3m2!1spt-BR!2sus!4v1479922746183",
		"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3986.0157675110113!2d-44.23001258524424!3d-2.5016882981770507!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7f6923d406861d1%3A0x9483b0749450232d!2sEmmarka+MA!5e0!3m2!1spt-BR!2sbr!4v1479923061809"
	];
	$endereco.text(adress[result]);
	$mapa.attr("src", map[result]);

 });
