<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'emmarka');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_8qnGqsMlc*V#3/LA0bLz[d}x{OmozR:5b9zrvaYYZ@b,#w2PcukPzBL8qxj(> `');
define('SECURE_AUTH_KEY',  '7:;Y#[!NSBR}%tmJM!A>%(L7,=Tx!w}(sy0%r/z;)EQ5aEbXN`S-b?_@9I2}1&%r');
define('LOGGED_IN_KEY',    'ZW<CpT4PWijY{cpD%qasfkj-Af(/4FGz2r~Wg_kKt_WLnm[/=Xc7j(40kH}w[o#d');
define('NONCE_KEY',        'uCIIp Xd:`YQn$]0OLfmJL69+F|WrwGYn/L7gu,N$k-@}-PP@kJ*$L@QlDdYC7M=');
define('AUTH_SALT',        '=K%*0hJ.W?U `].|<q02u,UP-bm&+xN$;q:T=($V|V,]Oqew4Qxm-G^@B*)xY7yt');
define('SECURE_AUTH_SALT', ']7M~f~l4rnN=MvRY#RP_*7y*d>-&!~?zC!U>{rUql0k1UcE4u?`%9W7i1qP_0~?R');
define('LOGGED_IN_SALT',   '7:C|Wr2>$#yewPky_yNV[0T0/d}7eoOOc2@<T!B)<e H>n]Ov[$[ZE?TS1}bB,?a');
define('NONCE_SALT',       'J5(z;sE8.lX U=R(p-DCaGsd*n|+i4#(GdVxp]5;}<nxkz^nr|8O7`j OKDNvQy3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
